package com.event.repository;

import com.event.entity.OrganizerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrganiseRepository extends JpaRepository<OrganizerEntity, Integer> {
}
