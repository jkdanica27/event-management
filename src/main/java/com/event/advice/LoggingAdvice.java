package com.event.advice;


import com.event.exception.InvalidTypeException;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;



@Aspect
@Component
public class LoggingAdvice {

   Logger log = LoggerFactory.getLogger(LoggingAdvice.class);
    @Pointcut(value="execution(* com.event.proj.*.*.*(..))")
    public void myPointCut(){
    }
  @Around("myPointCut()")
  public Object applicationLogger(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
            ObjectMapper mapper = new ObjectMapper();
            String methodName = proceedingJoinPoint.getSignature().getName();
            String className = proceedingJoinPoint.getTarget().getClass().toString();
            Object[] array = proceedingJoinPoint.getArgs();
            long startTime = System.currentTimeMillis();

          log.info("before method invoked : " + className + " ; " + methodName + "()" + "  arguments : " +
                  mapper.writeValueAsString(array));
      try {
          Object object = proceedingJoinPoint.proceed();
          long timeTaken = System.currentTimeMillis() - startTime;

          log.info("After method invoked : " + "ClassName : " + className + " ; " + methodName + "()" + "Status code : " + "  Response time :" + timeTaken + " ; " + "Response :" +
                  mapper.writeValueAsString(array));
          return object;
      }catch (InvalidTypeException ex){
          log.info(ex.getMessage());
          throw new InvalidTypeException("Something went wrong");
      }
    }
}
