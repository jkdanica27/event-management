package com.event.service;

import com.event.entity.EventsEntity;
import com.event.exception.BadRequestException;
import com.event.exception.EmptyListException;
import com.event.exception.NotFoundException;
import com.event.model.Events;
import com.event.repository.EventRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;
@Service
public class EventService {
    @Autowired
    EventRepository eventRepository;

    private ModelMapper modelMapper = new ModelMapper();


    Logger logger = LoggerFactory.getLogger(this.getClass());

    public List<EventsEntity> addDetails(List<Events> events)  {

        Type listType = new TypeToken<List<EventsEntity>>(){}.getType();
        List<EventsEntity> entities = modelMapper.map(events, listType);
        logger.info("event has been stored successfully");
        return eventRepository.saveAll(entities);
    }

    public List<Events> getDetails() throws NotFoundException,EmptyListException{

        List<EventsEntity> details = eventRepository.findAll();
        if(details==null ){
            logger.error("Error has Occurred.");
            throw new NotFoundException("no details is found","/events/getEventDetails");
        }
        if(details.isEmpty()){
            logger.error("Error has occurred.");
            throw new EmptyListException("There is no details to show","/events/getEventDetails");
      }
        Type listType = new TypeToken<List<Events>>(){}.getType();
        List<Events> events = modelMapper.map(details, listType);
        logger.info("successfully Retrieved.");
        return events;
    }

    public Events getById(int id) throws NotFoundException{
        EventsEntity detail = eventRepository.findById(id).orElseThrow(() ->
                new NotFoundException("no details is found", "/events/getEventDetails"));
        Events events = modelMapper.map(detail, Events.class);
        return events;
    }
    public EventsEntity updateDetails(int id, Events events) throws NotFoundException{
        EventsEntity detail = eventRepository.findById(id).orElseThrow(() ->
                new NotFoundException("no details is found", "/events/getEventDetails"));

        EventsEntity updatedRecord = modelMapper.map(events, EventsEntity.class);

       detail.setLocation(updatedRecord.getLocation());
       detail.setEventName(updatedRecord.getEventName());
       detail.setEventType(updatedRecord.getEventType());
       detail.setDescription(updatedRecord.getDescription());
       detail.setOrganizer(updatedRecord.getOrganizer());
       detail.setParticipants(updatedRecord.getParticipants());
       detail.setStartDate(updatedRecord.getStartDate());
       detail.setEndDate(updatedRecord.getEndDate());

       EventsEntity save = eventRepository.save(detail);
       logger.info("updated successfully..");
        return save;
    }

    public String deleteDetailbyId(int id) throws NotFoundException,BadRequestException {

        EventsEntity noDetailsIsFound = eventRepository.findById(id).orElseThrow(() ->
                new NotFoundException("no details is found", "/events/deleteEventDetails"));
                eventRepository.deleteById(id);
                logger.info("deleted successfully");
                return "This detail has been deleted";
    }

  public String deleteAll() throws EmptyListException,BadRequestException {
       List<EventsEntity> eventsEntities = eventRepository.findAll();
       if(eventsEntities != null) {
            eventRepository.deleteAll();
           return "All details has been deleted";
        }
        if(eventsEntities==null){
            logger.error("Error has occurred.");
           throw new EmptyListException("The list is empty.","");
       }
       else {
            logger.info("bad request");
            throw new BadRequestException("Something provided wrong look in that","/events/deleteAll");
       }
   }

    }
