package com.event.service;

import com.event.entity.OrganizerEntity;
import com.event.exception.EmptyListException;
import com.event.exception.NotFoundException;
import com.event.model.Organizer;
import com.event.repository.OrganiseRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Optional;

@Service
public class OrganiserService {
    @Autowired
    OrganiseRepository organiseRepository;

    Logger logger = LoggerFactory.getLogger(this.getClass());
    ModelMapper modelMapper = new ModelMapper();
    public List<Organizer> getDetails() throws NotFoundException, EmptyListException {

        List<OrganizerEntity> details = organiseRepository.findAll();
        if(details==null ){
            logger.error("Error has Occurred.");
            throw new NotFoundException("no details is found","/v1/organiser/getOrganiserDetails");
        }
        if(details.isEmpty()){
            logger.error("Error has Occurred.");
            throw new EmptyListException("There is no details to show","/v1/organiser/getOrganiserDetails");
        }
        Type listType = new TypeToken<List<Organizer>>(){}.getType();
        List<Organizer> organizers = modelMapper.map(details, listType);
        logger.info("successfully retrieved");
        return organizers;
    }
    public Organizer getById(int id) throws NotFoundException{

        Optional<OrganizerEntity> detail =organiseRepository.findById(id);
                if(detail.isPresent()) {
                    Organizer organizer = modelMapper.map(detail, Organizer.class);
                    return organizer;
                }
                else {
                    throw new NotFoundException("no details is found","/v1/organiser/getOrganiserDetails");
                }
    }

}
