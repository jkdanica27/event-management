package com.event.service;

import com.event.entity.ParticipantsEntity;
import com.event.exception.EmptyListException;
import com.event.exception.NotFoundException;
import com.event.model.Participant;
import com.event.repository.ParticipantRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class ParticipantService {
    @Autowired
    ParticipantRepository participantRepository;

     ModelMapper modelMapper = new ModelMapper();

    Logger logger = LoggerFactory.getLogger(this.getClass());



    public List<Participant> getDetails() throws NotFoundException, EmptyListException {

        List<ParticipantsEntity> details = participantRepository.findAll();
        if(details==null ){
            logger.error("Error has occurred.");
            throw new NotFoundException("no details is found","/v1/participants/getParticipantDetails");
        }
        if(details.isEmpty()){
            logger.error("Error has occurred.");
            throw new EmptyListException("There is no details to show","/v1/participants/getParticipantDetails");
        }
        Type listType = new TypeToken<List<Participant>>(){}.getType();
        List<Participant> participants = modelMapper.map(details, listType);
        logger.info("retrieved successfully...");
        return participants;
    }
    public Participant getById(int id) throws NotFoundException{
        ParticipantsEntity detail = participantRepository.findById(id).orElseThrow(() ->
                new NotFoundException("no details is found", "/v1/organiser/getOrganiserDetails"));
        Participant participant = modelMapper.map(detail, Participant.class);
        logger.info("data has been got..");
        return participant;
    }


}
