package com.event.controller;

import com.event.entity.EventsEntity;
import com.event.entity.OrganizerEntity;
import com.event.exception.InvalidTypeException;
import com.event.model.Events;
import com.event.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@Controller
@RequestMapping("/v1/event")
public class EventController {
    @Autowired
    EventService eventService;

    @PostMapping("/addEventDetails")
    public ResponseEntity<List<EventsEntity>> addEvent(@RequestBody List<Events> events) throws InvalidTypeException{
        List<EventsEntity> eventsEntities = eventService.addDetails(events);
        return new ResponseEntity<List<EventsEntity>>(eventsEntities,HttpStatus.CREATED);
    }
    @GetMapping("/getEventDetails")
    public ResponseEntity<List<Events>> getAllEvents() {

          return new ResponseEntity<>( eventService.getDetails(),HttpStatus.OK);

    }

    @GetMapping("/getEventDetails/{id}")
    public ResponseEntity<Events> getByid(@PathVariable int id)
    {
        return new ResponseEntity<>(eventService.getById(id),HttpStatus.OK);
    }


    @PutMapping("/updateEvent/{id}")
    public ResponseEntity<EventsEntity> updateEvent(@PathVariable int id, @RequestBody Events events){
            return new ResponseEntity<>(eventService.updateDetails(id,events),HttpStatus.ACCEPTED);
    }
    @DeleteMapping("/deleteEvent/{id}")
    public ResponseEntity<EventsEntity> deleteData(@PathVariable int id) {
         eventService.deleteDetailbyId(id);
         return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

   @DeleteMapping("/deleteAll")
   public ResponseEntity<OrganizerEntity> deleteAll(){
       eventService.deleteAll();
       return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
}
