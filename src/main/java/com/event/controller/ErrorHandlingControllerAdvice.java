package com.event.controller;

import com.event.exception.*;
import com.event.model.ResponseErrors;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.OffsetDateTime;

@ControllerAdvice
 class ErrorHandlingControllerAdvice  {

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    ResponseErrors onNotFoundException(NotFoundException e){
        ResponseErrors responseErrors = new ResponseErrors();
        responseErrors.setError(e.getMessage());
        responseErrors.setPath(e.getPath());
        responseErrors.setStatusCode(404);
        responseErrors.setTime(OffsetDateTime.now());
        return responseErrors;
    }

   @ExceptionHandler(InternalServerException.class)
   @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
   @ResponseBody
   ResponseErrors onInternalError(InternalServerException e){
        ResponseErrors responseErrors = new ResponseErrors();
        responseErrors.setError(e.getMessage());
        responseErrors.setStatusCode(500);
        responseErrors.setTime(OffsetDateTime.now());
        return responseErrors;
   }
    @ExceptionHandler(EmptyListException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    ResponseErrors onEmptyList(EmptyListException e){
        ResponseErrors responseErrors = new ResponseErrors();
        responseErrors.setError(e.getMessage());
        responseErrors.setPath(e.getPath());
        responseErrors.setStatusCode(606);
        responseErrors.setTime(OffsetDateTime.now());
        return responseErrors;
    }

    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    ResponseErrors onBadRequestException(NotFoundException e){
        ResponseErrors responseErrors = new ResponseErrors();
        responseErrors.setError(e.getMessage());
        responseErrors.setPath(e.getPath());
        responseErrors.setStatusCode(400);
        responseErrors.setTime(OffsetDateTime.now());
        return responseErrors;
    }

    @ExceptionHandler(InvalidTypeException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    ResponseErrors onInvalidTypeException(NotFoundException e){
        ResponseErrors responseErrors = new ResponseErrors();
        responseErrors.setError(e.getMessage());
        responseErrors.setPath(e.getPath());
        responseErrors.setStatusCode(205);
        responseErrors.setTime(OffsetDateTime.now());
        return responseErrors;
    }
}
