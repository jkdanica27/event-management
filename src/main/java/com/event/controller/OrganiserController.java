package com.event.controller;

import com.event.entity.OrganizerEntity;
import com.event.model.Organizer;
import com.event.service.OrganiserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/v1/organiser")
public class OrganiserController {
    @Autowired
    OrganiserService organiserService;

    @GetMapping("/getOrganiserDetails")
    public ResponseEntity<List<Organizer>> getAllEvents() {

        return new ResponseEntity<>( organiserService.getDetails(), HttpStatus.OK);

    }
    @GetMapping("/getOrganiserDetails/{id}")
    public ResponseEntity<Organizer> getByid(@PathVariable int id)
    {
        return new ResponseEntity<>(organiserService.getById(id),HttpStatus.OK);
    }

}
