package com.event.controller;

import com.event.entity.ParticipantsEntity;
import com.event.model.Participant;
import com.event.service.ParticipantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/v1/participants")
public class ParticipantController {
    @Autowired
    ParticipantService participantService;

    @GetMapping("/getParticipantDetails")
    public ResponseEntity<List<Participant>> getAllEvents() {

        return new ResponseEntity<>( participantService.getDetails(), HttpStatus.OK);

    }
    @GetMapping("/getParticipantDetails/{id}")
    public ResponseEntity<Participant> getByid(@PathVariable int id)
    {
        return new ResponseEntity<>(participantService.getById(id),HttpStatus.OK);
    }

}
