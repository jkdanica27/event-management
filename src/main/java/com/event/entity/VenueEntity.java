package com.event.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class VenueEntity implements Serializable {
    @Id
    @GeneratedValue
    private long id;
    @Column(name = "addressLine1")
    private String addressLine1;
    @Column(name="addressLine2")
    private String addressLine2;
    @Column(name = "city")
    private String city;
    @Column(name = "state")
    private String state;
    @Column(name = "country")
    private Country country;
    @Column(name = "postalCode")
    private String postalCode;

    public VenueEntity(int id, String addres1, String addres2, String coimbatore, String tamilnadu, String india, String postalCode) {
    }

    public enum Country{
        INDIA,
        USA,
        BRAZIL,
    }

}
