package com.event.entity;



import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

import java.io.Serializable;



@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class OrganizerEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true)
    private int id;
    @Column(name="Organiser_name",nullable = false)
    private String name;
    @Column(name = "contact",nullable = false)
    private String mobile;


}
