package com.event.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name="EVENT_DATA")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class EventsEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, updatable = false,nullable = false)
    private int id;
    @Column(name="Event_Name",nullable = false)
    private String eventName;
    @Column(name="Type",nullable = false)
    private String eventType;
    @Column(name="Description")
    private String description;
    @ManyToOne(cascade = CascadeType.ALL)
    private OrganizerEntity organizer;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "participants")
//    @JsonBackReference
    private List<ParticipantsEntity> participants = new ArrayList<>();
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private VenueEntity location;
    @Column(name="Start_Date",nullable = false)
    private String startDate;
    @Column(name="End_Date",nullable = false)
    private String endDate;



}
