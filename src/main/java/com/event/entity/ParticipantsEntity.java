package com.event.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import java.io.Serializable;


@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class ParticipantsEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = true)
    private int id;
    @Column(name = "participant_name")
    private String name;
    @Column(name="age")
    @Min(15)
    @Max(23)
    private int age;
    @Email(message = "invalid email format")
    @Column(name="emailId",unique = true)
    private String email;
    @Column(name="mobile_number")
    private String mobile;
    private boolean loggedIn;





}
