package com.event.model;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Venue {
    @JsonProperty("addressLine1")
    private String addressLine1;
    @JsonProperty("addressLine2")
    private String addressLine2;
    @JsonProperty("city")
    private String city;
    @JsonProperty("state")
    private String state;
    @Enumerated(EnumType.STRING)
    @JsonProperty("country")
    private Country country;
    @JsonProperty("postalCode")
    private String postalCode;

    public enum Country{
        INDIA,
        USA,
        BRAZIL,
    }

}
