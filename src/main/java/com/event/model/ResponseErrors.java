package com.event.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.OffsetDateTime;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ResponseErrors {
    private String error;
    private long statusCode;
    private String path;
    private OffsetDateTime time;
}
