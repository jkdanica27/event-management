package com.event.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Events {

    @NotBlank
    @JsonProperty("eventName")
    private String eventName;
    @JsonProperty("eventType")
    private String eventType;
    @JsonProperty("description")
    private String description;
    @JsonProperty("organizer")
    private Organizer organizer;
    private List<Participant> participants = new ArrayList<>();
    @JsonProperty("location")
    private Venue location;
   @JsonProperty("startDate")
//   @JsonFormat(shape=JsonFormat.Shape.STRING,pattern = "yyy-MM-dd")
    private String  startDate;
   @JsonProperty("endDate")
//   @JsonFormat(shape=JsonFormat.Shape.STRING,pattern = "yy-MM-dd")
    private String endDate;
}
