package com.event.servicetest;

import com.event.entity.ParticipantsEntity;
import com.event.exception.NotFoundException;
import com.event.model.Participant;
import com.event.repository.ParticipantRepository;
import com.event.service.ParticipantService;
import org.junit.jupiter.api.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
@Tag("UnitTest")
@DisplayName("Participant unit test")
public class ParticipantServiceTest {
    @MockBean
    private ParticipantRepository participantRepositoryMock;

    @Autowired
    private ParticipantService participantService;


    @Test
    void whenGetbyid_ThenRetrieved(){

        int existingId = 2;
        ParticipantsEntity part1 = ParticipantsEntity.builder()
                .id(2)
                .name("Gokul")
                .email("gokul@gmail.com")
                .age(22)
                .mobile("6381969678")
                .loggedIn(true)
                .build();

        //when
        Participant p1 = participantService.getById(existingId);

        //then
        assertNotNull(part1);
        assertNotNull(part1.getId());
        assertEquals(p1.getId(),part1.getId());
    }

//    @Test
//    void givenId_whenGetNotFound_ThenExceptionThrown(){
//        int id = 100;
//        String errormsg = "Not Found: "+id;
//        when(participantRepositoryMock.findById(id)).thenThrow(new NotFoundException(errormsg,"/getById"));
//
//        NotFoundException throwException = assertThrows(NotFoundException.class, () ->
//                participantService.getById(id));
//
//        assertEquals(errormsg,throwException.getMessage());
//    }

    @Test
    void whenList_ThenRetrieved(){

          int id = 3;
          ParticipantsEntity part1 = ParticipantsEntity.builder().id(id)
                  .name("name1")
                  .email("email1@gmail.com")
                  .age(22)
                  .mobile("6381969678")
                  .loggedIn(true)
                  .build();
          when(participantRepositoryMock.findAll()).thenReturn(Arrays.asList(part1));

        List<Participant> participants = participantService.getDetails();

        assertNotNull(participants);
        assertFalse(participants.isEmpty());
        assertEquals(part1.getId(),participants.get(0).getId());
    }

}
