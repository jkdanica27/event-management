package com.event.servicetest;

import com.event.entity.EventsEntity;
import com.event.entity.OrganizerEntity;
import com.event.entity.ParticipantsEntity;
import com.event.entity.VenueEntity;
import com.event.repository.EventRepository;
import com.event.service.EventService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
public class EventServiceTest {

    @Mock
    EventRepository eventRepository;

    @InjectMocks
    EventService eventService;

    @Test
    @DisplayName("creates eventlist")
    public void test_createEvents(){
        OrganizerEntity organisers = new OrganizerEntity(1,"Rajesh","9866805421");


        List<ParticipantsEntity> participants = new ArrayList<>();
        participants.add(new ParticipantsEntity(3,"Arun",22,"arun@gmail.com","6544789021",true));
        participants.add(new ParticipantsEntity(4,"Arav",21,"arav@gmail.com","6544789021",true));

        VenueEntity venue = new VenueEntity(5,"addres1","addres2","coimbatore","Tamilnadu","INDIA","641020");

        List<EventsEntity> events = new ArrayList<>(Arrays.asList(1,"event1","eventypeexample","description",
                organisers,participants,venue,"2022-05-10","2022-05-12"));
        eventService.addDetails(events);



    }

}
