package com.event.servicetest;

import com.event.entity.OrganizerEntity;
import com.event.model.Organizer;
import com.event.repository.OrganiseRepository;
import com.event.service.OrganiserService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
public class OrganizerServiceTest {

    @Mock
    OrganiseRepository organiseRepository;

    @InjectMocks
    OrganiserService organiserService;

    @Test
    @DisplayName("this test case should return all the organisers")
    public void test_getAllOrganiser(){
        List<OrganizerEntity> organisers = new ArrayList<OrganizerEntity>();
        organisers.add(new OrganizerEntity(1,"Rajesh","9866805421"));
        organisers.add(new OrganizerEntity(2,"Karthi","8081332435"));

        when(organiseRepository.findAll()).thenReturn(organisers);

        assertEquals(2,organiserService.getDetails().size());
    }

//   @Test
//   @DisplayName("This test should return organiser by id")
//    public void test_getByIdOrganiser(){
//        OngoingStubbing<Organizer> rajesh = when(organiserService.getById(1)).thenReturn(new OrganizerEntity(1, "Rajesh", "9866805421"));
//
//        OrganizerEntity organizerEntity = organiseRepository.getById(1);
//        assertEquals("Rajesh",organizerEntity.getName());
//    }


}
